interface IHasPrice {
    getPrice(): number;
}

// class Ramen implements IHasPrice {
//     getPrice() {
//         return 5;
//     }
// }
// class Spaghetti implements IHasPrice {
//     getPrice() {
//         return 10;
//     }
// }

interface IMenuComponent extends IHasPrice {
    getName(): string;
    hasAlergen(name: string): boolean;
}

export class AbsMenuComponentImpl implements IMenuComponent {

    public getPrice(): number {
        throw Error("Not implemented");
    }

    public getName(): string {
        throw Error("Not implemented");
    }

    public hasAlergen(name: string): boolean {
        throw Error("Not implemented");
    }

}
