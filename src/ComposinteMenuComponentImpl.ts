import {AbsMenuComponentImpl} from "./menu";

export class ComposinteMenuComponentImpl extends AbsMenuComponentImpl {
    private readonly name: string;
    private readonly items: AbsMenuComponentImpl[];
    constructor(name: string, items: AbsMenuComponentImpl[]) {
        super();
        this.name = name;
        this.items = items;
    }

    public addComponent(item: AbsMenuComponentImpl) {
        this.items.push(item);
    }
    public removeComponenmt(itemIndex: number) {
        delete this.items[itemIndex];
    }
    public getPrice() {
        return this.items.reduce((a, b) => a + b.getPrice(), 0);
    }
    public getComponent(itemIndex: number) {
        return this.items[itemIndex];
    }
    public hasAlergen(name: string) {
        for (const it of this.items) {
            if (it.hasAlergen(name)) {
                return true;
            }
        }
        return false;
    }

}
