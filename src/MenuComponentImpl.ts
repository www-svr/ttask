import {AbsMenuComponentImpl} from "./menu";

export class MenuComponentImpl extends AbsMenuComponentImpl {

    private readonly name: string;
    private readonly price: number;
    private readonly alergens: string[];

    constructor(name: string, price: number, alergens: string[]) {
        super();
        this.name = name;
        this.price = price;
        this.alergens = alergens;
    }

    public getName() {
        return this.name;
    }

    public getPrice() {
        return this.price;
    }

    public hasAlergen(name: string) {
        return this.alergens.indexOf(name) >= 0;
    }
}
