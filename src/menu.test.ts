import {ComposinteMenuComponentImpl} from "./ComposinteMenuComponentImpl";
import { MenuComponentImpl} from "./MenuComponentImpl";

describe("composite menu", () => {
    let ramen: MenuComponentImpl;
    let pork: MenuComponentImpl;
    let ramenWithPork: ComposinteMenuComponentImpl;
    beforeAll(() => {
        ramen = new MenuComponentImpl("Ramen", 5, []);
        pork = new MenuComponentImpl("Pork", 10, []);
        ramenWithPork = new ComposinteMenuComponentImpl("Ramen with Pork", [ramen, pork]);
    });

    it("should create custom items", () => {
        expect(ramenWithPork.getPrice()).toEqual(15);
    });

    it("should create custom nested items", () => {
        const juice = new MenuComponentImpl("Juice", 3, []);
        const ramenWithPorkAndJuice = new ComposinteMenuComponentImpl(
          "Ramen with Pork and Juice",
          [ramenWithPork, juice],
        );
        expect(ramenWithPorkAndJuice.getPrice()).toEqual(18);
    });
});

describe("alergens menu", () => {
    let itemWithAlerg: MenuComponentImpl;
    let anotherItemWithAlerg: MenuComponentImpl;
    beforeAll(() => {
        itemWithAlerg = new MenuComponentImpl("Item with alerg", 5, ["alerg1"]);
        anotherItemWithAlerg = new MenuComponentImpl("Item with alerg 2", 5, ["alerg2"]);
    });

    it("should detect alergens in simple items", () => {
        expect(itemWithAlerg.hasAlergen("alerg1")).toBeTruthy();
    });

    it("should detect alergens in composite items", () => {
        const compositeAlerg = new ComposinteMenuComponentImpl(
          "multiple alergs",
          [itemWithAlerg, anotherItemWithAlerg],
        );
        expect(compositeAlerg.hasAlergen("alerg1")).toBeTruthy();
    });

    it("should find only specified alergs", () => {
        expect(itemWithAlerg.hasAlergen("alerg3")).toBeFalsy();
    });

});
